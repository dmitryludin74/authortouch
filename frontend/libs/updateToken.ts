import axios from 'axios';

const { URL_SERVER_API } = process.env;

const updateToken = async ctx => {
    try {
        const { data } = await axios.get(`${URL_SERVER_API}/api/external/auth/refresh`, {
            withCredentials: true,
            headers: {
                Cookie: ctx.headers.cookie || ''
            }
        });

        return data;
    } catch (e) {
        console.log('Error update token')

        return {
            token: '',
            refreshToken: ''
        };
    }
};

export default updateToken;
