const cache = require('koa-redis-cache');

const { NODE_ENV } = process.env;

const dev = NODE_ENV !== 'production';

const cacheMiddleware = (ctx, next) => {
  const aid = ctx.cookies.get('aid');

  const options = {
    redis: {
      host: dev ? '127.0.0.1' : 'redis'
    },
    routes: [
      {
        path: '(.*)',
        expire: 600
      }
    ],
    exclude: ['/auth/(.*)', '/registration/(.*)', '/resetPassword/(.*)', '/promo', '/promo(.*)', '/ping']
  };

  if (!aid) {
    return cache(options)(ctx, next);
  }

  return next();
};

module.exports = cacheMiddleware;
