const Router = require('koa-router');

const cacheMiddleware = require('../middleware/cache.ts');

const initRoutes = (app, handle) => {
  const router = new Router();

  router.use(cacheMiddleware);

  router.get('/article/:id/:title', async (ctx) => {
    ctx.res.statusCode = 200;
    ctx.body = await app.render(ctx.req, ctx.res, '/article', { ...ctx.query, ...ctx.params });
    ctx.respond = false;
  });
  router.get(/.*/, async (ctx) => {
    const re = /^\/(static|public|_next)\/.+/g;
    const errorJSON = /.*\.hot-update.json/g;

    // if ((ctx.status === 404 && !re.test(ctx.path)) || errorJSON.test(ctx.path)) {
    //     await initialProps(ctx);
    // }

    await handle(ctx.req, ctx.res);
    ctx.respond = false;
  });

  return router.routes();
};

module.exports = initRoutes;
