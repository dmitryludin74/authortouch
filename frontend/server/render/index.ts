// import initialProps from '../utils/initialProps';

const index = async (app, ctx, url, page) => {
  try {
    // const { redirect } = await initialProps(ctx, page);

    // if (redirect && page !== 'ping') {
    //     ctx.status = 302;
    //     return ctx.redirect(redirect);
    // }

    ctx.res.statusCode = 200;

    ctx.body = await app.render(ctx.req, ctx.res, url, { ...ctx.query, ...ctx.params });
    ctx.respond = false;
  } catch (err) {
    ctx.status = 500;
    console.log('Some errors in render');
  }
};

export default index;
