const env = require('dotenv');
const Koa = require('koa');
const next = require('next');
const helmet = require('koa-helmet');
const cookie = require('koa-cookie').default;
const { DOMParser } = require('xmldom');

const nextConfig = require('../app/nextConfig.ts');
const initRoutes = require('./routes/index.ts');
const sessionCookie = require('./utils/sessionCookie.ts');

env.config({ path: '../.env' });

const { NODE_ENV, APP_PORT } = process.env;

const port = parseInt(APP_PORT, 10) || 3000;
const dev = NODE_ENV !== 'production';

const app = next({
  dev,
  dir: `${__dirname}/../app`,
  conf: nextConfig
});
const handle = app.getRequestHandler();

global.DOMParser = DOMParser;

const initServer = async () => {
  try {
    await app.prepare();

    const server = new Koa();

    server.use(
      helmet({
        frameguard: false,
        hsts: false
      })
    );

    server.use(cookie());

    server.use(sessionCookie);

    server.use(initRoutes(app, handle));

    server.use(async (ctx, next) => {
      ctx.res.statusCode = 200;
      await next();
    });

    server.use(handle);

    server.listen(port, '0.0.0.0', (err) => {
      if (err) throw err;

      // eslint-disable-next-line no-console
      console.log(`> Ready on http://localhost:${port}`);
    });
  } catch (e) {
    throw new Error(e);
  }
};

initServer().then(() => console.log('> Youauthor is started'));
