const axios = require('axios');

const { URL_SERVER_API } = process.env;

/** TODO: добавлять поддержку языков здесь!!! */

const initialProps = async (ctx, page) => {
  const {
    data: { articles }
  } = await axios(`${URL_SERVER_API}/api/external/articles`, {
    //TODO: Тут будем получать список всех статей, если вдруг ошибка, и произойдет редирект на главную
    withCredentials: true,
    headers: {
      Cookie: ctx.headers.cookie || ''
    }
  });

  try {
    const {
      data: { user }
    } = await axios.get(`${URL_SERVER_API}/api/external/auth/info/`, {
      //TODO: Тут получаем информацию о пользователе
      withCredentials: true,
      headers: {
        Cookie: ctx.headers.cookie || ''
      }
    });

    ctx.req.profile = user || {};

    return {
      articles
    };
  } catch (e) {
    if (e.response && e.response.status === 401) {
      return {
        redirect: '/'
      };
    }

    return {
      articles
    };
  }
};

module.exports = initialProps;
