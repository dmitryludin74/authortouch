const { nanoid } = require('nanoid');

async function sessionCookie(ctx, next) {
  const htmlPage =
    !ctx.url.match(/^\/(_next|static|public)/) && !ctx.url.match(/\.(js|map)$/) && ctx.accepts('text/html', 'text/css', 'image/png') === 'text/html';
  const cookies = ctx.cookie;

  if (!htmlPage) {
    await next();
    return;
  }

  if (cookies && (!cookies.sid || cookies.sid.length === 0)) {
    cookies.sid = nanoid();
    ctx.cookies.set('sid', cookies.sid);
  }

  await next();
}

module.exports = sessionCookie;
