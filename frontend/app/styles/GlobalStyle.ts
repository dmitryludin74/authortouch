import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
    *,
    *::before,
    *::after {
      box-sizing: border-box;
    }
    
    ul,
    ol {
      padding: 0;
    }
    
    body, h1, h2,
    h3, h4, p,
    ul, ol,
    li, figure,
    figcaption, blockquote,
    dl, dd {
      margin: 0;
    }

    // html {
    //   font-size: 62.5%;
    // }
    
    body {
      min-height: 100vh;
      font-family: Roboto;
      font-style: normal;
      font-weight: 500;
      font-size: max(.83vw, 13px);
      height: 100%;
      scroll-behavior: smooth;
      text-rendering: optimizeSpeed;
      line-height: 1.5;
    }
    
    ul,
    ol {
      list-style: none;
    }
    
    a:not([class]) {
      text-decoration-skip-ink: auto;
    }
    
    img {
      max-width: 100%;
      display: block;
    }
    
    article > * + * {
      margin-top: 1em;
    }
    
    input,
    button,
    textarea,
    select {
      font: inherit;
    }
`;

export default GlobalStyle;
