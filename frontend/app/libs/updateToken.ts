import axios from 'axios';
import { setCookie, destroyCookie } from 'nookies';

import { buildUrl } from './utils';

const updateToken = async () => {
  try {
    const options = { httpOnly: true, sameSite: 'lax' };
    const { data } = await axios.get(buildUrl('auth/refresh'), {
      withCredentials: true
    });

    setCookie(null, 'aid', data.token, options);
    setCookie(null, 'rid', data.refreshToken, options);
  } catch (e) {
    destroyCookie(null, 'aid');
    destroyCookie(null, 'rid');
  }
};

export default updateToken;
