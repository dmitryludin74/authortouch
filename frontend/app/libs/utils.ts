import getConfig from 'next/config';

const { publicRuntimeConfig } = getConfig();
const { serverApiUrl, localApiUrl } = publicRuntimeConfig;

export const isServer = typeof window === 'undefined';

export const buildUrl = (url) => {
  const serverLink = `${serverApiUrl}/api/external/${url}`;
  const localLink = `${localApiUrl}/api/external/${url}`;

  return isServer ? serverLink : localLink || `${document.location.origin}/api/external/${url}`;
};
