// @ts-ignore
require('dotenv').config();
const path = require('path');
const Dotenv = require('dotenv-webpack');

const { URL_LOCAL_WEB, URL_SERVER_API, NODE_ENV } = process.env;

module.exports = {
  dir: 'app',
  poweredByHeader: false,
  compress: false,
  typescript: {
    ignoreBuildErrors: true
  },
  pageExtensions: ['mdx', 'jsx', 'js', 'ts', 'tsx'],
  webpack: (config, { buildId, dev, isServer, defaultLoaders, webpack }) => {
    config.plugins = config.plugins || [];

    config.node = {
      fs: 'empty'
    };

    config.module.rules.push({
      test: /(\/|\\)sprite(\/|\\)(.*)\.svg$/,
      use: [
        {
          loader: 'svg-sprite-loader'
        },
        'svgo-loader'
      ]
    });

    config.module.rules.push({
      test: /(\/|\\)normal(\/|\\)(.*)\.svg$/,
      use: '@svgr/webpack'
    });

    config.plugins = [
      ...config.plugins,
      new Dotenv({
        path: path.join(__dirname, '../.env'),
        systemvars: true
      })
    ];

    return config;
  },
  publicRuntimeConfig: {
    localApiUrl: URL_LOCAL_WEB,
    serverApiUrl: URL_SERVER_API,
    isProduction: NODE_ENV === 'production'
  }
};
