import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    wrapper: {
      display: 'flex',
      justifyContent: 'flex-end',
      alignItems: 'center',
      minWidth: theme.typography.pxToRem(220),
      flexGrow: 2
    },
    rootButton: {
      boxShadow: '0px 0px 1px rgba(117, 131, 142, 0.04), 0px 2px 4px rgba(52, 60, 68, 0.16)',
      borderRadius: theme.typography.pxToRem(8),
      padding: '8px 12px',
      fontStyle: 'normal',
      fontWeight: theme.typography.fontWeightRegular,
      fontSize: theme.typography.pxToRem(16),
      lineHeight: theme.typography.pxToRem(16),
      marginRight: theme.spacing(2),
      textTransform: 'none'
    }
  })
);
