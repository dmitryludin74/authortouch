import React, { FC } from 'react';

import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';

import { useStyles } from './AuthButtons.styled';

const AuthButtons: FC = () => {
  const classes = useStyles();

  return (
    <Box className={classes.wrapper}>
      <Button className={classes.rootButton}>Войти</Button>
      <Button className={classes.rootButton} color="primary" variant="contained">
        Регистрация
      </Button>
    </Box>
  );
};

export default AuthButtons;
