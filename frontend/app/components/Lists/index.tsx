import React, { FC, ReactElement } from 'react';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListSubheader from '@material-ui/core/ListSubheader';
import Tooltip from '@material-ui/core/Tooltip';

import { useStyles } from './Lists.styled';

interface ILists {
  items: [{ id: string; icon: ReactElement; label?: string; tooltipText?: string }];
  title?: string | ReactElement;
  gutters?: number;
  tooltip?: boolean;
}

const Lists: FC<ILists> = ({ items, tooltip, title, gutters }) => {
  const classes = useStyles({ gutters });

  return (
    <List subheader={title && <ListSubheader className={classes.listSubheader}>{title}</ListSubheader>}>
      {items.map((item, i) => (
        <Tooltip
          arrow
          disableTouchListener={!tooltip}
          disableHoverListener={!tooltip}
          disableFocusListener={!tooltip}
          placement="right"
          key={item.id + i}
          title={item.tooltipText}
        >
          <ListItem button className={classes.listItemStyled} disableGutters>
            {item.icon && <ListItemIcon className={classes.listIcon}>{item.icon}</ListItemIcon>}
            {item.label && <ListItemText>{item.label}</ListItemText>}
          </ListItem>
        </Tooltip>
      ))}
    </List>
  );
};

export default Lists;
