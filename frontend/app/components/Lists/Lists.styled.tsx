import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    listSubheader: {
      ...theme.typography.subtitle1,
      padding: '8px 16px',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      lineHeight: 1.5
    },
    listItemStyled: {
      justifyContent: 'center',
      padding: '8px 16px'
    },
    listIcon: {
      minWidth: 'auto',
      paddingRight: (props) => props.gutters
    }
  })
);
