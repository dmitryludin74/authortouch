import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    header: {
      padding: `0 ${theme.spacing(2.25)}px`
    },
    button: {
      padding: 4
    }
  })
);
