import React, { FC } from 'react';

import Menu from '../Menu';
import Search from '../Search';
import AuthButtons from '../AuthButtons';
import User from '../User';
import Logo from '../Logo';
import AppBar from '@material-ui/core/AppBar';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';

import { useStyles } from './Header.styled';

interface IHeader {
  toggleMenu: () => void;
}

const Header: FC<IHeader> = ({ toggleMenu }) => {
  const classes = useStyles();

  return (
    <AppBar className={classes.header} color={'inherit'} position={'fixed'}>
      <Grid spacing={2} wrap="nowrap" justify="space-between" alignItems="center" container>
        <Grid item>
          <Grid spacing={5} wrap="nowrap" alignItems="center" container>
            <Grid item>
              <IconButton className={classes.button} aria-controls="customized-menu" aria-haspopup="true" onClick={toggleMenu}>
                <MenuIcon color="primary" fontSize="large" />
              </IconButton>
            </Grid>
            <Grid item>
              <Logo />
            </Grid>
            <Grid item>
              <Menu />
            </Grid>
          </Grid>
        </Grid>
        <Grid item>
          <Search />
        </Grid>
        <Grid item>
          <Grid wrap="nowrap" alignItems="center" container>
            <Grid item>
              <AuthButtons />
            </Grid>
            <Grid item>
              <User />
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </AppBar>
  );
};

export default Header;
