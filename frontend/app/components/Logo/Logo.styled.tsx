import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    logo: {
      width: '36px',
      height: '36px',
      backgroundColor: '#C2C2C2',
      borderRadius: '50%'
    }
  })
);
