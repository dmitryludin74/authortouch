import React, { FC } from 'react';

import { useStyles } from './Logo.styled';

const Logo: FC = () => {
  const classes = useStyles();

  return <div className={classes.logo} />;
};

export default Logo;
