import React, { FC } from 'react';

import UserMenuAuth from './UserMenuAuth/UserMenuAuth';

const User: FC = () => {
  return (
    <>
      <UserMenuAuth />
    </>
  );
};

export default User;
