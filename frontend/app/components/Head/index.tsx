import React, { FC } from 'react';
import NextHead from 'next/head';

const Head: FC = () => (
  <NextHead>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
  </NextHead>
);

export default Head;
