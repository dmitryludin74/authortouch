import React, { FC } from 'react';

import ThumbUpAltIcon from '@material-ui/icons/ThumbUpAlt';
import WatchLaterIcon from '@material-ui/icons/WatchLater';
import BookmarkIcon from '@material-ui/icons/Bookmark';
import ImportContactsIcon from '@material-ui/icons/ImportContacts';
import BookIcon from '@material-ui/icons/Book';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import MenuOpenIcon from '@material-ui/icons/MenuOpen';

import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import IconButton from '@material-ui/core/IconButton';
import Logo from '../Logo';
import SidebarMenu from '../Lists';
import Drawer from '@material-ui/core/Drawer';
import Divider from '@material-ui/core/Divider';

import { useStyles } from './SidebarDrawer.styled';

const mock1 = [
  { id: String(Date.now()), icon: <BookIcon style={{ fontSize: 22 }} />, label: 'Библиотека' },
  { id: String(Date.now()), icon: <ImportContactsIcon style={{ fontSize: 22 }} />, label: 'Продолжить чтение' },
  { id: String(Date.now()), icon: <BookmarkIcon style={{ fontSize: 22 }} />, label: 'История' },
  { id: String(Date.now()), icon: <WatchLaterIcon style={{ fontSize: 22 }} />, label: 'Понравившееся' },
  { id: String(Date.now()), icon: <ThumbUpAltIcon style={{ fontSize: 22 }} />, label: 'Закладки' }
];

const mock2 = [
  { id: String(Date.now()), icon: <AccountCircleIcon style={{ fontSize: 34 }} />, label: 'Автор' },
  { id: String(Date.now()), icon: <AccountCircleIcon style={{ fontSize: 34 }} />, label: 'Автор' },
  { id: String(Date.now()), icon: <AccountCircleIcon style={{ fontSize: 34 }} />, label: 'Автор' }
];

const mock3 = [
  { id: String(Date.now()), icon: <AccountCircleIcon style={{ fontSize: 34 }} />, label: 'Автор' },
  { id: String(Date.now()), icon: <AccountCircleIcon style={{ fontSize: 34 }} />, label: 'Автор' },
  { id: String(Date.now()), icon: <AccountCircleIcon style={{ fontSize: 34 }} />, label: 'Автор' }
];
interface ISidebarDrawer {
  isOpen: boolean;
  toggleMenu: () => void;
}

const SidebarDrawer: FC<ISidebarDrawer> = ({ isOpen, toggleMenu }) => {
  const classes = useStyles();

  return (
    <Drawer open={isOpen} onClose={toggleMenu} elevation={16}>
      <Grid component="nav" direction="column" container>
        <Grid item>
          <Box paddingLeft={2.25} p={0.7}>
            <Grid spacing={5} wrap="nowrap" alignItems="center" container>
              <Grid item>
                <IconButton className={classes.button} aria-controls="customized-menu" aria-haspopup="true" onClick={toggleMenu}>
                  <MenuOpenIcon color="primary" fontSize="large" />
                </IconButton>
              </Grid>
              <Grid item>
                <Logo />
              </Grid>
            </Grid>
          </Box>
        </Grid>
        <Divider />
        <Grid item>
          <SidebarMenu gutters={16} items={mock1} />
        </Grid>
        <Divider />
        <Grid item>
          <SidebarMenu gutters={16} items={mock2} title="Отслеживаемые авторы" />
        </Grid>
        <Divider />
        <Grid item>
          <SidebarMenu gutters={16} items={mock3} title="Рекомендуемые авторы" />
        </Grid>
      </Grid>
    </Drawer>
  );
};

export default SidebarDrawer;
