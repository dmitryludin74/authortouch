import React, { FC } from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import List from '@material-ui/core/List';
import ListItemText from '@material-ui/core/ListItemText';

import { useStyles } from './SidebarMenu.styled';

interface ISidebarMenu {
  items: [{ id: string; customeNode: Element; label?: string }];
  button: boolean | true;
}

const SidebarMenu: FC<ISidebarMenu> = ({ items, button }) => {
  const classes = useStyles();

  return (
    <>
      <List className={classes.listWrapper}>
        {items.map((item, i) => (
          <ListItem className={classes.listItemStyled} disableGutters key={item.id + i} button={button && i === 0 ? false : true}>
            {item.customeNode && <ListItemIcon className={classes.listItemIconStyled}>{item.customeNode}</ListItemIcon>}
            <ListItemText>{item.label}</ListItemText>
          </ListItem>
        ))}
      </List>
    </>
  );
};

export default SidebarMenu;
