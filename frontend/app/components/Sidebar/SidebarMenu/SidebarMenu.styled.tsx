import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    listWrapper: {
      padding: '8px 0'
    },
    listItemIconStyled: {
      display: 'flex',
      justifyContent: 'center'
    },
    listItemStyled: {
      padding: '8px'
    }
  })
);
