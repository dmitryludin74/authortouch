import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    sidebarWrapper: {
      position: 'fixed',
      top: 50,
      left: 0,
      width: '72px',
      height: '100vh',
      overflow: 'hidden auto'
    }
  })
);
