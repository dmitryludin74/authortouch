import React, { FC } from 'react';

import Lists from '../Lists';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';

import ThumbUpAltIcon from '@material-ui/icons/ThumbUpAlt';
import WatchLaterIcon from '@material-ui/icons/WatchLater';
import BookmarkIcon from '@material-ui/icons/Bookmark';
import ImportContactsIcon from '@material-ui/icons/ImportContacts';
import BookIcon from '@material-ui/icons/Book';
import SupervisorAccountIcon from '@material-ui/icons/SupervisorAccount';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';

import { useStyles } from './Sidebar.styled';

const mock1 = [
  {
    tooltipText: 'Библиотека',
    id: Date.now(),
    icon: <BookIcon style={{ fontSize: 22 }} />
  },
  { id: Date.now(), tooltipText: 'Продолжить чтение', icon: <ImportContactsIcon style={{ fontSize: 22 }} /> },
  { id: Date.now(), tooltipText: 'История', icon: <BookmarkIcon style={{ fontSize: 22 }} /> },
  { id: Date.now(), tooltipText: 'Понравившееся', icon: <WatchLaterIcon style={{ fontSize: 22 }} /> },
  { id: Date.now(), tooltipText: 'Закладки', icon: <ThumbUpAltIcon style={{ fontSize: 22 }} /> }
];

const mock2 = [
  { id: Date.now(), tooltipText: 'Aвтор', icon: <AccountCircleIcon style={{ fontSize: 34 }} /> },
  { id: Date.now(), tooltipText: 'Aвтор', icon: <AccountCircleIcon style={{ fontSize: 34 }} /> },
  { id: Date.now(), tooltipText: 'Aвтор', icon: <AccountCircleIcon style={{ fontSize: 34 }} /> }
];

const mock3 = [
  { id: Date.now(), tooltipText: 'Aвтор', icon: <AccountCircleIcon style={{ fontSize: 34 }} /> },
  { id: Date.now(), tooltipText: 'Aвтор', icon: <AccountCircleIcon style={{ fontSize: 34 }} /> },
  { id: Date.now(), tooltipText: 'Aвтор', icon: <AccountCircleIcon style={{ fontSize: 34 }} /> }
];

const Sidebar: FC = () => {
  const classes = useStyles();

  return (
    <Paper square className={classes.sidebarWrapper} elevation={2}>
      <Lists tooltip items={mock1} />
      <Divider />
      <Lists tooltip title={<SupervisorAccountIcon style={{ fontSize: 24 }} />} items={mock2} />
      <Divider />
      <Lists tooltip title={<PersonAddIcon style={{ fontSize: 24 }} />} items={mock3} />
    </Paper>
  );
};

export default Sidebar;
