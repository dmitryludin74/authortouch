import React, { FC, ChangeEvent } from 'react';

import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

import { useStyles } from './Menu.styled';

const Menu: FC = () => {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event: ChangeEvent<Event>, newValue: number) => {
    setValue(newValue);
  };

  return (
    <Tabs value={value} onChange={handleChange} indicatorColor="primary" textColor="primary">
      <Tab className={classes.tab} disableRipple label="Лента" />
      <Tab className={classes.tab} disableRipple label="Категории" />
    </Tabs>
  );
};

export default Menu;
