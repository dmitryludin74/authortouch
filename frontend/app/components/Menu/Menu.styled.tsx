import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    tab: {
      minWidth: 'auto',
      padding: 0,
      marginRight: `${theme.spacing(3)}px`,
      fontSize: theme.typography.pxToRem(20),
      textTransform: 'capitalize'
    }
  })
);
