import React, { FC } from 'react';

import SearchIcon from '@material-ui/icons/Search';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import Button from '@material-ui/core/Button';

import { useStyles } from './Search.styled';

const Search: FC = () => {
  const classes = useStyles();

  return (
    <Paper square={false} elevation={0} component="form" className={classes.root}>
      <InputBase className={classes.input} placeholder="Поиск" />
      <Button className={classes.button} variant="contained" color="primary" aria-label="search">
        <SearchIcon className={classes.icon} />
      </Button>
    </Paper>
  );
};

export default Search;
