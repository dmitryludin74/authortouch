import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      padding: '4px 0 4px 16px',
      height: 38,
      display: 'flex',
      alignItems: 'center',
      width: theme.typography.pxToRem(500),
      borderRadius: 8,
      backgroundColor: theme.palette.background.default
    },
    input: {
      marginLeft: theme.spacing(1),
      flex: 1,
      fontSize: theme.typography.pxToRem(20),
      fontWeight: 500,
      color: theme.palette.text.primary
    },
    button: {
      minWidth: 32,
      borderRadius: 8
    },
    icon: {
      fontSize: theme.typography.pxToRem(28)
    }
  })
);
