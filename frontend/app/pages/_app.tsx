import React, { useEffect } from 'react';
import type { AppProps } from 'next/app';
import { Provider } from 'react-redux';
import Router, { useRouter } from 'next/router';
import axios from 'axios';
import NProgress from 'nprogress';
import store from '../redux/store';
import CssBaseline from '@material-ui/core/CssBaseline';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';

import updateToken from '../libs/updateToken';

Router.events.on('routeChangeStart', () => NProgress.start());
Router.events.on('routeChangeComplete', () => NProgress.done());
Router.events.on('routeChangeError', () => NProgress.done());

function MyApp({ Component, pageProps }: AppProps) {
  const router = useRouter();
  const prefersDarkMode = useMediaQuery('(prefers-color-scheme: dark)');

  const theme = React.useMemo(
    () =>
      createMuiTheme({
        palette: {
          type: prefersDarkMode ? 'dark' : 'light'
        }
      }),
    [prefersDarkMode]
  );

  useEffect(() => {
    axios.interceptors.response.use(null, async (error) => {
      const {
        config,
        response: { status }
      } = error;

      if (status === 401) {
        try {
          await updateToken();
          return axios(config);
        } catch (e) {
          await router.push('/');
          return Promise.reject(error);
        }
      }

      return Promise.reject(error);
    });
  }, [router]);

  return (
    <ThemeProvider theme={theme}>
      <Provider store={store}>
        <CssBaseline />
        <Component {...pageProps} />
      </Provider>
    </ThemeProvider>
  );
}

// MyApp.getInitialProps = async ({ Component, ctx }) => {
//   // const { req } = ctx;
//   // const { articles, profile } = req;
//   //
//   // ctx.profile = profile;
//   // ctx.articles = articles;
//
//   try {
//     const pageProps = Component.getInitialProps ? await Component.getInitialProps(ctx) : {};
//
//     return {
//       pageProps
//       // articles,
//       // profile
//     };
//   } catch (error) {
//     if (error.response && error.response.status !== 404) {
//       console.log('Error in _app.js', error);
//     }
//
//     return {
//       error: null
//     };
//   }
// };
//
export default MyApp;
