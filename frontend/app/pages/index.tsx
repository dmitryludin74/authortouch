import React, { FC } from 'react';
import Router from 'next/router';

import Button from '@material-ui/core/Button/Button';
import Typography from '@material-ui/core/Typography';
import MainLayout from '../layout/MainLayout';

const Home: FC = () => {
  const onClickHandler = () => Router.push('/test');

  return (
    <MainLayout>
      <Typography variant="h2" component="h2">
        My page
      </Typography>

      <Button variant={'outlined'} onClick={onClickHandler}>
        На Material UI
      </Button>
      <Typography variant="h2" component="h2">
        My page
      </Typography>

      <Button variant={'outlined'} onClick={onClickHandler}>
        На Material UI
      </Button>
      <Typography variant="h2" component="h2">
        My page
      </Typography>

      <Button variant={'outlined'} onClick={onClickHandler}>
        На Material UI
      </Button>
      <Typography variant="h2" component="h2">
        My page
      </Typography>

      <Button variant={'outlined'} onClick={onClickHandler}>
        На Material UI
      </Button>
      <Typography variant="h2" component="h2">
        My page
      </Typography>

      <Button variant={'outlined'} onClick={onClickHandler}>
        На Material UI
      </Button>
      <Typography variant="h2" component="h2">
        My page
      </Typography>

      <Button variant={'outlined'} onClick={onClickHandler}>
        На Material UI
      </Button>
      <Typography variant="h2" component="h2">
        My page
      </Typography>

      <Button variant={'outlined'} onClick={onClickHandler}>
        На Material UI
      </Button>
      <Typography variant="h2" component="h2">
        My page
      </Typography>

      <Button variant={'outlined'} onClick={onClickHandler}>
        На Material UI
      </Button>
      <Typography variant="h2" component="h2">
        My page
      </Typography>

      <Button variant={'outlined'} onClick={onClickHandler}>
        На Material UI
      </Button>
      <Typography variant="h2" component="h2">
        My page
      </Typography>

      <Button variant={'outlined'} onClick={onClickHandler}>
        На Material UI
      </Button>
      <Typography variant="h2" component="h2">
        My page
      </Typography>

      <Button variant={'outlined'} onClick={onClickHandler}>
        На Material UI
      </Button>
      <Typography variant="h2" component="h2">
        My page
      </Typography>

      <Button variant={'outlined'} onClick={onClickHandler}>
        На Material UI
      </Button>
      <Typography variant="h2" component="h2">
        My page
      </Typography>

      <Button variant={'outlined'} onClick={onClickHandler}>
        На Material UI
      </Button>
      <Typography variant="h2" component="h2">
        My page
      </Typography>

      <Button variant={'outlined'} onClick={onClickHandler}>
        На Material UI
      </Button>
      <Typography variant="h2" component="h2">
        My page
      </Typography>

      <Button variant={'outlined'} onClick={onClickHandler}>
        На Material UI
      </Button>
      <Typography variant="h2" component="h2">
        My page
      </Typography>

      <Button variant={'outlined'} onClick={onClickHandler}>
        На Material UI
      </Button>
    </MainLayout>
  );
};

export default Home;
