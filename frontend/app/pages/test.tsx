import React, { FC } from 'react';
import Router from 'next/router';

import Button from '@material-ui/core/Button/Button';
import Typography from '@material-ui/core/Typography';
import MainLayout from '../layout/MainLayout';

const Test: FC = () => {
  const onClickHandler = () => Router.push('/');

  return (
    <MainLayout>
      <Typography variant="h2" component="h2">
        My page
      </Typography>
      <Button variant={'outlined'} onClick={onClickHandler}>
        На ANTD
      </Button>
    </MainLayout>
  );
};

export default Test;
