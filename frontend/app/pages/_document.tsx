import Document, { DocumentContext, Html, Head, Main, NextScript } from 'next/document';
import { ServerStyleSheets, createMuiTheme, responsiveFontSizes } from '@material-ui/core/styles';
import sprite from 'svg-sprite-loader/runtime/sprite.build';

const theme = responsiveFontSizes(createMuiTheme());

export default class MyDocument extends Document {
  static async getInitialProps(ctx: DocumentContext) {
    const sheet = new ServerStyleSheets();
    const originalRenderPage = ctx.renderPage;
    const spriteContent = sprite.stringify();

    try {
      ctx.renderPage = () =>
        originalRenderPage({
          enhanceApp: (App) => (props) => sheet.collect(<App {...props} />)
        });

      const initialProps = await Document.getInitialProps(ctx);

      return {
        ...initialProps,
        spriteContent,
        styles: (
          <>
            {initialProps.styles}
            {sheet.getStyleElement()}
          </>
        )
      };
    } finally {
      console.log('test');
    }
  }

  render() {
    const { spriteContent } = this.props;
    return (
      <Html lang="ru">
        <Head>
          <meta charSet="utf-8" />
          {/* <meta name="viewport" content="minimum-scale=1, initial-scale=1, width=device-width, shrink-to-fit=no" /> */}
          <meta name="theme-color" content={theme.palette.primary.main} />
          <style jsx global>
            {`
              *,
              *::before,
              *::after {
                box-sizing: border-box;
              }

              ul,
              ol {
                padding: 0;
              }

              body,
              h1,
              h2,
              h3,
              h4,
              p,
              ul,
              ol,
              li,
              figure,
              figcaption,
              blockquote,
              dl,
              dd {
                margin: 0;
              }

              html,
              body {
                min-height: 100vh;
                height: 100%;
                font-family: Roboto;
                font-style: normal;
                font-weight: 500;
                font-size: max(0.83vw, 13px);
                scroll-behavior: smooth;
                text-rendering: optimizeSpeed;
                line-height: 1.5;
                overflow: auto;
              }

              ul,
              ol {
                list-style: none;
              }

              a:not([class]) {
                text-decoration-skip-ink: auto;
              }

              img {
                max-width: 100%;
                display: block;
              }

              article > * + * {
                margin-top: 1em;
              }

              input,
              button,
              textarea,
              select {
                font: inherit;
              }
            `}
          </style>
        </Head>
        <body>
          <div className="sprite" dangerouslySetInnerHTML={{ __html: spriteContent }} />
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}
