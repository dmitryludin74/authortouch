import React, { FC, useState } from 'react';

import Container from '@material-ui/core/Container';
import Head from '../components/Head';
import Sidebar from '../components/Sidebar';
import Header from '../components/Header';
import SidebarDrawer from '../components/SidebarDrawer';

import { useStyles } from './MainLayout.styled';

const MainLayout: FC = ({ children }) => {
  const classes = useStyles();
  const [isOpen, setIsOpen] = useState(false);

  function toggleDrawer() {
    setIsOpen(!isOpen);
  }

  return (
    <>
      <Head />
      <Header toggleMenu={toggleDrawer} />
      <SidebarDrawer toggleMenu={toggleDrawer} isOpen={isOpen} />
      <Sidebar />
      <Container maxWidth={false} className={classes.content} disableGutters>
        {children}
      </Container>
    </>
  );
};

export default MainLayout;
