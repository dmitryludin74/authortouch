import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    content: {
      padding: '74px 24px 24px 96px',
      height: '100%',
      backgroundColor: theme.palette.background.default
    }
  })
);
