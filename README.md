# YouAuthor

Социальная сеть для писателей

## Структура проекта

* **deploy** — Скрипты для запуска контейнеров
* **server** — Бекэнд и API
* **frontend** — Интерфейс для управления

Далее все пути для конкретного модуля указаны относительно директории,
в которой он находится.

Конфигурация каждого модуля делается в файлах `.env`.
Эти файлы находится в игноре и никогда не комитится в репозиторий.

## Frontend

Пример `.env`

```
APP_PORT=3500
URL_LOCAL_WEB=http://yourIP:8085
URL_SERVER_API=http://yourIP:8085
```

Установка и запуск

```
$ npm install
$ npm dev
```
Или

```
$ yarn
$ yarn dev
```

## Deploy

В папке нужно создать файл `.env`:

```
MYSQL_DATABASE=youauthor
MYSQL_USER=app
MYSQL_PASSWORD=password
MYSQL_ROOT_PASSWORD=password
```

### Запуск и остановка Nginx, MySQL и Redis

```
./deploy/start-all.sh
./deploy/stop-all.sh
```

### Сервер API

https://youauthor.local

Пример `.env`

```
DEBUG=youauthor:*
MYSQL_DATABASE=youauthor
MYSQL_USER=app
MYSQL_PASSWORD=password
MYSQL_HOST=127.0.0.1
MYSQL_PORT=3306
REDIS_HOST=127.0.0.1
REDIS_PORT=6380
CREATE_REACT_APP_URL=http://127.0.0.1:3006/
API_PORT=8085
```

Запросы проксируются через Nginx на локальный сервер, который запущен на 3500 порту.

В `/etc/hosts` нужно добавить:

```
127.0.0.1 youauthor.local
::1 youauthor.local
```

Установка и запуск

```
$ npm install
$ npm run nodemon
```
Или

```
$ yarn
$ yarn nodemon
```

Создание базы данных:

```
sequelize-cli db:create --charset utf8 --collate utf8_unicode_ci
sequelize-cli db:migrate
sequelize-cli db:seed:all
```
