#!/usr/bin/env bash

cd "${0%/*}"

docker-compose -p authorthouch \
  -f mysql.yml \
  -f redis.yml \
  -f nginx.yml \
  stop
