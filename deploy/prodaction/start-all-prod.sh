#!/usr/bin/env bash
#mysql & redis from deploy directory
cd "${0%/*}"

docker-compose -p authortouch \
  -f mysql.yml \
  -f redis.yml \
  -f app.yml \
  --env-file .env \
  up -d --build
