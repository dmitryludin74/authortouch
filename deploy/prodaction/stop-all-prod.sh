#!/usr/bin/env bash

cd "${0%/*}"

docker-compose -p authortouch \
  -f mysql.yml \
  -f redis.yml \
  -f app.yml \
  stop
