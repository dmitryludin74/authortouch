class ForbiddenError extends Error {
  static get NAME() {
    return 'ForbiddenError';
  }

  constructor(message: any) {
    super(message || 'Forbidden');
    Error.captureStackTrace(this, ForbiddenError);
    this.name = ForbiddenError.NAME;
    this.status = 403;
  }
}

module.exports = ForbiddenError;
