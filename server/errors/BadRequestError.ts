class BadRequestError extends Error {
  static get NAME() {
    return 'BadRequestError';
  }

  constructor(message: string) {
    super(message);
    Error.captureStackTrace(this, BadRequestError);
    this.name = BadRequestError.NAME;
    this.status = 400;
  }
}

module.exports = BadRequestError;
