const { promisify } = require('util');
const env = require('dotenv');
// @ts-ignore
const debug = require('debug')('youauthor:app');
const Koa = require('koa');
const serve = require('koa-static');
const jsonp = require('koa-jsonp');
const koaBody = require('koa-body');
const path = require('path');
const userAgent = require('koa2-useragent');
// @ts-ignore
const router = require('./router/index.ts');
// const errorMiddleware = require('./middlewares/error');
const db = require('./db/index.ts');

env.config({ path: './.env' });

const { API_PORT } = process.env;

const port: number = parseInt(API_PORT, 10) || 8085;

const app = new Koa({
  proxy: true
});

// Обработчик ошибок
// app.use(errorMiddleware);

app.use(userAgent());

app.use(serve(path.resolve(__dirname, './public')));

app.use(jsonp());
app.use(koaBody());

// Использование роутов
app.use(router.routes());
app.use(router.allowedMethods());

const listen = promisify(app.listen.bind(app));
// app.listen(port, '0.0.0.0', (err: any) => {
//   if (err) {
//     throw err;
//   }
//
//   debug(`Server is ready on port ${port} `);
// });
// Запускается сервер, выводит сообщение в консоль, нужно добавить базу данных
db.authenticate()
  .then(() => listen(port))
  .then(() => {
    debug('server is ready');
  })
  .catch((err: any) => {
    debug(err);
    process.exit(1);
  });
