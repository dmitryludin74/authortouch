module.exports = {
  up: async (queryInterface, { DataTypes }) => {
    await queryInterface.sequelize.transaction(async (transaction) => {
      await queryInterface.createTable(
        'users',
        {
          id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            autoIncrement: true,
            primaryKey: true
          },
          username: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true
          },
          name: {
            type: DataTypes.STRING,
            allowNull: false
          },
          email: {
            type: DataTypes.STRING,
            allowNull: false
          },
          created_at: {
            type: DataTypes.DATE,
            allowNull: false
          },
          updated_at: {
            type: DataTypes.DATE,
            allowNull: false
          },
          deleted_at: {
            type: DataTypes.DATE
          }
        },
        {
          charset: 'utf8',
          collate: 'utf8_general_ci',
          transaction
        }
      );

      await queryInterface.createTable(
        'user_secrets',
        {
          id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            autoIncrement: true,
            primaryKey: true
          },
          user_id: {
            type: DataTypes.INTEGER,
            references: {
              model: 'users',
              key: 'id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE'
          },
          salt: {
            type: DataTypes.STRING(64),
            allowNull: false
          },
          hash: {
            type: DataTypes.STRING(1024),
            allowNull: false
          },
          created_at: {
            type: DataTypes.DATE,
            allowNull: false
          },
          updated_at: {
            type: DataTypes.DATE,
            allowNull: false
          },
          deleted_at: {
            type: DataTypes.DATE
          }
        },
        {
          charset: 'utf8',
          collate: 'utf8_general_ci',
          transaction
        }
      );
    });
  },
  down: async (queryInterface) => {
    await queryInterface.sequelize.transaction(async (transaction) => {
      await queryInterface.dropTable('user_secrets', { transaction });
      await queryInterface.dropTable('users', { transaction });
    });
  }
};
