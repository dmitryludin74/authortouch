require('dotenv').config();

const { MYSQL_DATABASE, MYSQL_USER, MYSQL_PASSWORD, MYSQL_PORT, MYSQL_HOST } = process.env;

if (!MYSQL_USER || !MYSQL_PASSWORD || !MYSQL_DATABASE || !MYSQL_PORT || !MYSQL_HOST) {
  throw new Error('DB configuration is not defined');
}

module.exports = {
  username: MYSQL_USER,
  password: MYSQL_PASSWORD,
  database: MYSQL_DATABASE,
  host: MYSQL_HOST,
  port: MYSQL_PORT,
  dialect: 'mysql',
  migrationStorageTableName: 'sequelize_meta',
  define: {
    // default options for the Model.init
    timestamps: true,
    paranoid: true,
    underscored: true,
    charset: 'utf8',
    collate: 'utf8_general_ci'
  }
};
