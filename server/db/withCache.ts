const SequelizeSimpleCache = require('sequelize-simple-cache');

// initialize cache Взяты из действующего проекта. нужно будет заменить на модели для нашего проекта
module.exports = new SequelizeSimpleCache(
  {
    banner: { ttl: 300 },
    audioBanner: { ttl: 300 },
    bannerContent: { ttl: 300 },
    audioBannerContent: { ttl: 300 },
    bannerGeo: { ttl: 300 },
    bannerProvider: { ttl: 300 },
    bannerUserAgent: { ttl: 300 },
    extraOptions: { ttl: 300 },
    audioBannerGeo: { ttl: 300 },
    audioBannerProvider: { ttl: 300 },
    audioBannerUserAgent: { ttl: 300 },
    slot: { ttl: 300 },
    site: { ttl: 300 },
    slotSpecification: { ttl: 300 },
    action: { ttl: 300 }
  },
  {
    debug: false
  }
);
