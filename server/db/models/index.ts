const fs = require('fs');
// @ts-ignore
const path = require('path');
// @ts-ignore
const sequelize = require('../index');
const withCache = require('../withCache');

const basename = path.basename(__filename);

const models = {};

function createClassName(name) {
  return name[0].toUpperCase() + name.substr(1);
}

fs.readdirSync(__dirname)
  .filter((file) => file.indexOf('.') !== 0 && file !== basename && file.slice(-3) === '.ts')
  .forEach((file) => {
    const model = withCache.init(sequelize['import'](path.join(__dirname, file)));
    //const model = sequelize.import(path.join(__dirname, file));
    models[createClassName(model.name)] = model;
  });

Object.keys(models).forEach((modelName) => {
  if (models[modelName].associate) {
    models[modelName].associate(models);
  }
});

models.sequelize = sequelize;

module.exports = models;
