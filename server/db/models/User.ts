// @ts-ignore
const { Op, where, fn, col } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('user', {
    username: {
      type: DataTypes.STRING,
      allowNull: false
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        isEmail: true
      }
    },
    follow: {
      type: DataTypes.STRING,
      allowNull: true
    },
    assignedRoles: {
      type: DataTypes.VIRTUAL,
      get() {
        const { roles } = this;

        if (!Array.isArray(roles)) {
          return roles;
        }

        return roles.map((role) => role.name);
      }
    }
  });

  User.associate = ({ UserSecret, Role, Book }) => {
    User.hasOne(UserSecret, { as: 'secret', foreignKey: 'user_id' });
    User.hasMany(Book, { through: 'user_books', timestamps: false });
    User.belongsToMany(Role, { through: 'user_roles', timestamps: false });
  };

  /**
   * Find a user by username
   * @param {String} username
   * @param {Object} [options]
   * @return {Promise<Model | null>}
   */
  User.findByUsername = (username, options = {}) =>
    User.findOne({
      ...options,
      where: where(fn('lower', col('username')), {
        [Op.eq]: `${username}`.toLowerCase()
      })
    });

  return User;
};
