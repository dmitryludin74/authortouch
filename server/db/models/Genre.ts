module.exports = (sequelize, DataTypes) => {
  const Genre = sequelize.define('book', {
    id: {
      type: DataTypes.STRING(32),
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    description: {
      type: DataTypes.STRING,
      allowNull: true
    },
    preview: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        isUrl: true
      }
    }
  });

  Genre.associate = ({ Book, User, Story }) => {
    Genre.belongsToMany(User, { through: 'user_books', timestamps: false });
    Genre.belongsToMany(Book, { through: 'genre_books', timestamps: false });
    Genre.belongsToMany(Story, { through: 'genre_books', timestamps: false });
  };

  return Genre;
};
