module.exports = (sequelize, DataTypes) => {
  const Paragraph = sequelize.define('paragraph', {
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    title: {
      type: DataTypes.STRING,
      allowNull: true
    },
    fullId: {
      type: DataTypes.VIRTUAL,
      get() {
        return `${this.id}/${this.updatedAt}`;
      }
    }
  });

  Paragraph.associate = ({ Page, Chapter }) => {
    Paragraph.hasMany(Page);
    Paragraph.belongsTo(Chapter);
  };

  return Paragraph;
};
