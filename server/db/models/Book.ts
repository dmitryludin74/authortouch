module.exports = (sequelize, DataTypes) => {
  const Book = sequelize.define('book', {
    id: {
      type: DataTypes.STRING(32),
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    description: {
      type: DataTypes.STRING,
      allowNull: true
    },
    preview: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        isUrl: true
      }
    },
    isPublic: {
      type: DataTypes.BOOLEAN
    }
  });

  Book.associate = ({ Chapter, User, Genre }) => {
    Book.belongsTo(User, { through: 'user_books', timestamps: false });
    Book.hasMany(Chapter);
    Book.hasMany(Genre);
  };

  return Book;
};
