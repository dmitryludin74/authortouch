module.exports = (sequelize, DataTypes) => {
  const UserSecret = sequelize.define('userSecret', {
    salt: {
      type: DataTypes.STRING(64),
      allowNull: false
    },
    hash: {
      type: DataTypes.STRING(1024),
      allowNull: false
    }
  });

  return UserSecret;
};
