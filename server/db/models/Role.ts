// @ts-ignore
const { Op, where, fn, col } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  const Role = sequelize.define(
    'role',
    {
      name: {
        type: DataTypes.STRING(32),
        allowNull: false,
        unique: true
      }
    },
    {
      timestamps: false
    }
  );

  /**
   * Find a role by name
   * @param {String} name
   * @return {Promise<Model | null>}
   */
  Role.findByName = (name) =>
    Role.findOne({
      where: where(fn('lower', col('name')), {
        [Op.eq]: `${name}`.toLowerCase()
      })
    });

  /**
   * Match roles by the given list of the names
   * @param {Array<String>}names
   * @return {Promise<Array<Model>>}
   */
  Role.findByNames = (names) =>
    Promise.all(
      names.map(async (name) => {
        const role = await Role.findByName(name);

        if (!role) {
          throw new Error(`Role "${name}" is not found`);
        }

        return role;
      })
    );

  return Role;
};
