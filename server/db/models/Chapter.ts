module.exports = (sequelize, DataTypes) => {
  const Chapter = sequelize.define('chapter', {
    id: {
      type: DataTypes.STRING(32),
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    title: {
      type: DataTypes.STRING,
      allowNull: false
    }
  });

  Chapter.associate = ({ Book, Paragraph, Page }) => {
    Chapter.belongsTo(Book);
    Chapter.hasMany(Paragraph);
    Chapter.hasMany(Page);
  };

  /**
   * Find a slot by unitname and site
   * @param {String} name
   * @param {String} bookId
   * * @param {Object} [options]
   * @return {Promise<Model | null>}
   */
  Chapter.findOneByNameAndBook = (name, bookId, options = {}) =>
    Chapter.findOne({
      ...options,
      where: {
        name
      },
      include: [
        {
          model: sequelize.models.book,
          where: {
            id: bookId
          }
        }
      ]
    });

  return Chapter;
};
