module.exports = (sequelize, DataTypes) => {
  const Page = sequelize.define('page', {
    id: {
      type: DataTypes.STRING(32),
      primaryKey: true
    },
    content: {
      type: DataTypes.STRING(),
      validate: {
        isUrl: true
      }
    }
  });

  Page.associate = ({ Story, Paragraph, Chapter }) => {
    Page.belongsTo(Paragraph);
    Page.belongsTo(Chapter);
    Page.belongsTo(Story);
  };

  return Page;
};
