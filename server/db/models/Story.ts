module.exports = (sequelize, DataTypes) => {
  const Story = sequelize.define('book', {
    id: {
      type: DataTypes.STRING(32),
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    description: {
      type: DataTypes.STRING,
      allowNull: true
    },
    genre: {
      type: DataTypes.STRING,
      allowNull: false
    },
    isPublic: {
      type: DataTypes.BOOLEAN
    }
  });

  Story.associate = ({ Page, User, Genre }) => {
    Story.belongsTo(User, { through: 'user_books', timestamps: false });
    Story.hasMany(Page);
    Story.hasMany(Genre);
  };

  return Story;
};
