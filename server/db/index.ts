// @ts-ignore
const debug = require('debug')('adsbyzina:db');
const Sequelize = require('sequelize');
const config = require('./config');

const sequelize = new Sequelize({
  ...config,
  logging: (msg: any) => debug(msg)
});

module.exports = sequelize;
