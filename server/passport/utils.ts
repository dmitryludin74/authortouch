const { promisify } = require('util');
// @ts-ignore
const { Buffer } = require('buffer');
const crypto = require('crypto');

const randomBytes = promisify(crypto.randomBytes);
const pbkdf2 = promisify(crypto.pbkdf2);

const SALT_LENGTH = 32;
const KEY_LENGTH = 512;
const ITERATIONS = 25000;
const DIGEST_ALGORITHM = 'sha256';

/**
 * Compute salt and hash
 * @param {String} password
 * @param {Function} [cb]
 * @return {Promise<{salt, hash}>}
 */
function hashPassword(password: any, cb: (arg0: any, arg1: { salt: any; hash: any }) => any) {
  const promise = Promise.resolve()
    .then(() => randomBytes(SALT_LENGTH))
    .then((saltBuffer) => Promise.all([saltBuffer, pbkdf2(password, saltBuffer, ITERATIONS, KEY_LENGTH, DIGEST_ALGORITHM)]))
    .then((buffers) => buffers.map((b) => b.toString('hex')))
    .then(([salt, hash]) => ({
      salt,
      hash
    }));

  if (cb) {
    promise.then((result) => cb(null, result)).catch((err) => cb(err));
  }

  return promise;
}

/**
 * Validate existing data
 * @param {String} password
 * @param {String} salt
 * @param {String} hash
 * @param {Function} [cb]
 * @return {Promise<Boolean>}
 */
function validatePassword(password: any, salt: string, hash: string, cb: (arg0: any, arg1: undefined) => any) {
  const promise = Promise.resolve()
    .then(() => {
      const saltBuffer = Buffer.from(salt, 'hex');

      return pbkdf2(password, saltBuffer, ITERATIONS, KEY_LENGTH, DIGEST_ALGORITHM);
    })
    .then((generatedHashBuffer) => {
      const hashBuffer = Buffer.from(hash, 'hex');

      return crypto.timingSafeEqual(generatedHashBuffer, hashBuffer);
    });

  if (cb) {
    promise.then((result) => cb(null, result)).catch((err) => cb(err));
  }

  return promise;
}

module.exports = {
  hashPassword,
  validatePassword
};
