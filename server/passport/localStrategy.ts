const { callbackify } = require('util');
const LocalStrategy = require('passport-local');
// @ts-ignore
const { validatePassword } = require('./utils.ts');
// @ts-ignore
const { User, UserSecret, Role } = require('../db/models');
// @ts-ignore
const sanitizeUser = require('./sanitizeUser.ts');

async function checkCredentials(username: any, password: any) {
  const user = await User.findByUsername(username, {
    include: [
      {
        model: UserSecret,
        as: 'secret',
        attributes: ['id', 'salt', 'hash']
      },
      Role
    ]
  });

  if (!user || !user.secret) {
    return null;
  }

  const { salt, hash } = user.secret;
  const validationResult = await validatePassword(password, salt, hash);

  return validationResult ? sanitizeUser(user) : null;
}

module.exports = new LocalStrategy({}, callbackify(checkCredentials));
