const passport = require('passport');
const localStrategy = require('./localStrategy.ts');
const bearerStrategy = require('./bearerStrategy.ts');

passport.use(localStrategy);
passport.use(bearerStrategy);

/**
 * Authentication middleware
 * @param {String} [strategy]
 * @param {Object} [options]
 * @return {function}
 */
function authenticate(strategy = 'bearer', options = {}) {
  return passport.authenticate(strategy, {
    session: false,
    failWithError: true,
    ...options
  });
}

module.exports = authenticate;
