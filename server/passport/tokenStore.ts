const Redis = require('ioredis');

const { REDIS_PORT, REDIS_HOST } = process.env;

if (!REDIS_PORT || !REDIS_HOST) {
  throw new Error('Redis configuration is not defined');
}

const redis = new Redis({
  port: process.env.REDIS_PORT,
  host: process.env.REDIS_HOST,
  keyPrefix: 'youauthor:token:'
});

// @ts-ignore
function getItem(token: any) {
  return redis.get(token);
}

function setItem(token: any, content: any, ttl = 7 * 24 * 60 * 60) {
  return redis.set(token, content, ['ex', ttl]);
}

function removeItem(token: any) {
  return redis.del(token);
}

module.exports = {
  getItem,
  setItem,
  removeItem
};
