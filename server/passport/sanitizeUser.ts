const omit = require('lodash/omit');

// @ts-ignore
function sanitizeUser(user: { toJSON: () => any }) {
  return omit(user.toJSON(), ['roles', 'secret', 'createdAt', 'updatedAt', 'deletedAt']);
}

module.exports = sanitizeUser;
