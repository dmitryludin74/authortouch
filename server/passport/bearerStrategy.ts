const { Strategy } = require('passport-http-bearer');
// @ts-ignore
const { User, Role } = require('../db/models');
// @ts-ignore
const { getItem } = require('./tokenStore.ts');
// @ts-ignore
const sanitizeUser = require('./sanitizeUser.ts');

async function checkToken(token: any) {
  const userId = await getItem(token);

  if (!userId) {
    return null;
  }

  const user = await User.findByPk(userId, {
    include: [Role]
  });

  if (!user) {
    return null;
  }

  return sanitizeUser(user);
}

// eslint-disable-next-line max-len
module.exports = new Strategy({}, (token: any, done: { (arg0: any, arg1: any, arg2: { token: any }): any; (reason: any): PromiseLike<never> }) => {
  checkToken(token)
    .then((user) => done(null, user, { token }))
    .catch(done);
});
