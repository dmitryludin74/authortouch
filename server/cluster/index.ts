const index = require('cluster');

const { cpus } = require('os');
const start = require('./serverJob.ts');

const numCPUs = cpus().length;

if (index.isMaster) {
  for (let i = 0; i < numCPUs; i += 1) {
    index.fork();
  }

  index.on('online', (worker: { process: { pid: any } }) => {
    console.log(`Worker ${worker.process.pid} is online`);
  });

  index.on('exit', (worker: { process: { pid: any } }, code: any, signal: any) => {
    console.log(`Worker ${worker.process.pid} died with code: ${code} and signal: ${signal}`);
    console.log('Starting a new worker...');
    index.fork();
  });
} else {
  start();
}
