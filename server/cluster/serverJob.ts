// @ts-ignore
const debug = require('debug')('youauthor:app');
// @ts-ignore
const { promisify } = require('util');
// @ts-ignore
const Koa = require('koa');
// @ts-ignore
const serve = require('koa-static');
// @ts-ignore
const jsonp = require('koa-jsonp');
// @ts-ignore
const koaBody = require('koa-body');
const proxy = require('koa-proxies');
// @ts-ignore
const path = require('path');
// @ts-ignore
const userAgent = require('koa2-useragent');
// const router = require('./router');
// const errorMiddleware = require('./middlewares/error');
const db = require('../db/index.ts');

// @ts-ignore
function start(): void {
  const app = new Koa({
    proxy: true
  });

  // Обработчик ошибок
  // app.use(errorMiddleware);

  app.use(userAgent());

  app.use(serve(path.resolve(__dirname, './public')));

  app.use(jsonp());
  app.use(koaBody());

  // Использование роутов
  // app.use(router.routes());
  // app.use(router.allowedMethods());

  if (process.env.CREATE_REACT_APP_URL) {
    app.use(
      proxy('/', {
        target: process.env.CREATE_REACT_APP_URL
      })
    );
  } else {
    app.use((ctx: any, next: any) => serve(path.resolve(__dirname, './public'))(Object.assign(ctx, { path: 'index.html' }), next));
  }

  const listen = promisify(app.listen.bind(app));

  // Запускается сервер, выводит сообщение в консоль, нужно добавить базу данных
  db.authenticate()
    .then(() => listen(3500))
    .then(() => {
      debug('server is ready');
    })
    .catch((err: any) => {
      debug(err);
      process.exit(1);
    });
}
module.exports = start;
